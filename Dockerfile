FROM python:2.7

# Create app directory
WORKDIR /app

# Install app dependencies

RUN pip install requests

# Bundle app source
COPY mac.py /app

CMD [ "python", "mac.py" ]
